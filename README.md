# How to reproduce
1. Add the bitnami repo with `helm repo add bitnami https://charts.bitnami.com/bitnami`
1. Ensure you have the latest if it was already added with `helm repo update`
1. Load the two dependencies `helm dependency update somechart` and a somechart/charts/*tgz should appear with the correct templates and versions both
1. Package your chart with `helm package somechart` and it will generate your weird chart tarball
1. Inspect the tarball nginx charts and enjoy
1. (Bonus) Untar in another directory the previous packaged chart and see what happens
